from django.apps import AppConfig


class BitacoraConfig(AppConfig):
    name = 'core.bitacora'
