from django.forms import *
from core.bitacora.models import *


class NuevaPersona(ModelForm):
    class Meta:
        model = Personas
        field = '__all__'
        exclude = ['id']
        widgets = {
            'nombre': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control',
                    'rows': 3,
                    'cols': 3
                }

            ),
            'apellido': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'tipo_documento': Select(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control select2',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'cedula': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'correo': EmailInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'telefono': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),

        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class EditPersona(ModelForm):
    class Meta:
        model = Personas
        field = '__all__'
        exclude = ['id']
        widgets = {
            'nombre': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control',
                    'rows': 3,
                    'cols': 3
                }

            ),
            'apellido': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'tipo_documento': Select(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control select2',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'cedula': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'correo': EmailInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'telefono': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }
            ),

        }


class BitacoraForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Bitacoras
        field = '__all__'
        exclude = ['id', 'estado']
        widgets = {
            'reunion': TextInput(
                attrs={
                    'placeholder': 'Ingrese el nombre de la reunion',
                    'class': 'form-control ',
                    'rows': 3,
                    'cols': 3
                }

            ),
            'numero_personas': TextInput(
                attrs={
                    'placeholder': 'Numero de asistentes',
                    'class': 'form-control',
                    'rows': 3,
                    'cols': 3
                }
            ),
            'lugar_encuentro': Select(
                attrs={
                    'class': 'form-control select2',
                    'style': 'width: 100%'

                }
            ),
            'observaciones': Textarea(
                attrs={
                    'placeholder': 'Escriba las observaciones',
                    'class': 'form-control',
                    'rows': 3,
                    'cols': 3

                }
            ),
            'usuario': Select(
                attrs={
                    'class': 'form-control select2',
                    'style': 'width: 100%'
                }

            ),
            'fecha': DateInput(attrs={
                'type': 'date',
                'class': 'form-control'
            })

        }
