from django.contrib.auth.models import AbstractUser
from django.db import models
from datetime import datetime

from django.forms import model_to_dict

from config.settings import MEDIA_URL, STATIC_URL


class TipoDocumento(models.Model):
    tipo_documento = models.CharField(max_length=70)

    def __str__(self):
        return self.tipo_documento

    def cambioJson(self):
        item = model_to_dict(self)
        return item

    class Meta:
        db_table = 'TipoDocumento'
        verbose_name = "TipoDocumento"
        verbose_name_plural = "TipoDocumentos"


class user(AbstractUser):
    tipo_documento = models.ForeignKey(TipoDocumento, on_delete=models.CASCADE, null=True)
    numero_documento = models.CharField(max_length=10, unique=True)
    avatar = models.ImageField(upload_to='avartar/%Y/%m/%d', null=True, blank=True)

    def get_avatar(self):
        if self.avatar:
            return '{}{}'.format(MEDIA_URL, self.avatar)
        return '{}{}'.format(STATIC_URL, 'img/empty.png')

    def __str__(self):
        return self.get_full_name()

    def cambioJson(self):
        item = model_to_dict(self, exclude=['avatar'])
        return item


class Personas(models.Model):
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    tipo_documento = models.ForeignKey(TipoDocumento, on_delete=models.CASCADE)
    cedula = models.CharField(max_length=150, unique=True)
    correo = models.EmailField(max_length=200, unique=True)
    telefono = models.CharField(max_length=15)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.nombre, self.apellido)
        return full_name.strip()

    def __str__(self):
        return self.get_full_name()

    def cambioJson(self):
        item = model_to_dict(self)
        item['tipo_documento'] = self.tipo_documento.cambioJson()
        return item

    class Meta:
        db_table = "persona"
        verbose_name = "Persona"
        verbose_name_plural = "Personas"

    # Funcion para cambiar los datos a Diccionario para que se puedan manejar por Json


class lugar_encuentro(models.Model):
    lugar = models.CharField(max_length=150)

    def __str__(self):
        return self.lugar

    def cambioJson(self):
        item = model_to_dict(self)
        return item

    class Meta:
        db_table = "lugar_encuentro"
        verbose_name = "lugar_encuentro"
        verbose_name_plural = "lugar_encuentros"


class Bitacoras(models.Model):
    usuario = models.ForeignKey(user, on_delete=models.CASCADE)
    reunion = models.CharField(max_length=500)
    numero_personas = models.CharField(max_length=5)
    fecha = models.DateField(default=datetime.now)
    lugar_encuentro = models.ForeignKey(lugar_encuentro, on_delete=models.CASCADE)
    observaciones = models.TextField(null=True, blank=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.reunion

    def cambioJson(self):
        item = model_to_dict(self, exclude=['estado'])
        item['usuario'] = self.usuario.cambioJson()
        item['lugar_encuentro'] = self.lugar_encuentro.cambioJson()
        return item

    class Meta:
        db_table = 'bitacora'
        verbose_name_plural = 'Bitacoras'
        verbose_name = "Bitacora"


class DetalleBitacora(models.Model):
    bitacora = models.ForeignKey(Bitacoras, on_delete=models.CASCADE)
    persona = models.ForeignKey(Personas, on_delete=models.CASCADE)
    tapabocas = models.BooleanField(default=True)
    distancia = models.BooleanField(default=True)
    observaciones = models.TextField(null=True, blank=True)


    def __str__(self):
        return self.persona

    def cambioJson(self):
        item = model_to_dict(self)
        item['bitacora'] = self.bitacora.cambioJson()
        item['persona'] = self.persona.cambioJson()
        return item


    class Meta:
        db_table = "DetalleBitacora"
        verbose_name = "Detalle"
        verbose_name_plural = "Detalles"
