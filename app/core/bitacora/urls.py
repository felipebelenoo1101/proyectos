from django.urls import path

from core.bitacora.views.bitacora.views import BitacoraCreatedView, BitacoraListView, BitacoraUpdateView, \
    BitacoraDetallePdfView, BitacoraDeleteView
from core.bitacora.views.dashboard.views import DashboardView
from core.bitacora.views.personas.views import *

app_name = 'app'

urlpatterns = [
    path('listapersonas/', personasListView.as_view(), name='lista_personas'),
    path('nuevapersona/', PersonaCreateView.as_view(), name='Nueva_Persona'),
    # se envian parametros por ulr  en los casos de eliminar y actualizar se envia
    # la llave primaria
    path('editPersona/<int:pk>/', PersonaUpdate.as_view(), name='Editar_Persona'),
    path('deletePersona/<int:pk>/', PersonaDeleteView.as_view(), name='Eliminar_Persona'),
    # Dashboard
    path('', DashboardView.as_view(), name='Dashboard'),
    # Bitacora
    path('bitacoraadd/', BitacoraCreatedView.as_view(), name='bitacora'),
    path('bitacoralista/', BitacoraListView.as_view(), name='ListarBitacora'),
    path('bitacoraedit/<int:pk>/', BitacoraUpdateView.as_view(), name='EditarBitacora'),
    path('bitacorapdf/<int:pk>/', BitacoraDetallePdfView.as_view(), name='ImpresionDetalle'),
    path('bitacoradelete/<int:pk>/', BitacoraDeleteView.as_view(), name='EliminarBitacora'),

]
