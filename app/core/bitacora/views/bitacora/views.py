import json

# Libreria de PFDS
import os
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.contrib.staticfiles import finders

from datetime import *
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse, HttpResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, ListView, UpdateView, View, DeleteView

from core.bitacora.forms import BitacoraForm
from core.bitacora.models import Bitacoras, Personas, DetalleBitacora, user


class BitacoraListView(LoginRequiredMixin, ListView):
    model = Bitacoras
    template_name = 'bitacora/ListadoBitacora.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # Se envia la variable data dentro de un diccionario
        data = {}
        try:
            action = request.POST['action']
            if action == 'buscarDatos':
                data = []
                for i in Bitacoras.objects.all():
                    data.append(i.cambioJson())
            elif action == 'buscarDetalle':
                data = []
                for i in DetalleBitacora.objects.filter(bitacora_id=request.POST['id']):
                    data.append(i.cambioJson())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Listado de bitacoras'
        context['home'] = reverse_lazy('app:Dashboard')
        context['create_url'] = reverse_lazy('app:bitacora')
        context['status_url'] = reverse_lazy('app:ListarBitacora')
        context['status'] = 'lista de personas'
        context['bienvenido'] = 'Bienvenido'
        context['hora'] = datetime.now().ctime()
        context['datos'] = user.objects.all().order_by('id')
        return context


class BitacoraCreatedView(LoginRequiredMixin, CreateView):
    model = Bitacoras
    form_class = BitacoraForm
    template_name = 'bitacora/crearbitacora.html'
    success_url = reverse_lazy('app:Dashboard')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # Data es la variable que vamos a llevar al front para
        # interactuar con Ajax
        data = {}
        # Try ejecuta la accion y genera excepciones ante los errores
        try:
            # La variable Action evalua si lleva un atributo action el template
            # entonces se ejecuta la siguiente accion
            action = request.POST['action']
            # Si en el templeate action tiene el valor de Search_persons entonces guarda la data en un listado
            if action == 'search_persons':
                data = []
                # Se crea la variable clientes para enviar la tabla personas al templeate filtrado por nombre
                personas = Personas.objects.filter(nombre__icontains=request.POST['busqueda'])[0:5]
                # se recorre la variable para obtener todos los datos
                for i in personas:
                    # se envian los datos sen formato Json
                    item = i.cambioJson()
                    item['value'] = i.nombre
                    data.append(item)
            elif action == 'add':
                vents = json.loads(request.POST['vents'])
                bitacora = Bitacoras()
                bitacora.usuario_id = vents['usuario']
                bitacora.reunion = vents['reunion']
                bitacora.numero_personas = vents['numero_personas']
                bitacora.fecha = vents['fecha']
                bitacora.lugar_encuentro_id = vents['lugar_encuentro']
                bitacora.observaciones = vents['observaciones']
                bitacora.save()
                for i in vents['personas']:
                    detalle = DetalleBitacora()
                    detalle.bitacora_id = bitacora.id
                    detalle.persona_id = i['id']
                    detalle.tapabocas = bool(i['tapabocas'])
                    detalle.distancia = bool(i['distancia'])
                    detalle.observaciones = i['observacionesper']
                    detalle.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)

        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Nuevo Registro'
        context['home'] = reverse_lazy('app:Dashboard')
        context['status_url'] = reverse_lazy('')
        context['status'] = 'Crear registro'
        context['bienvenido'] = 'Crear Registro'
        context['detalle'] = []
        context['action'] = 'add'
        return context


class BitacoraUpdateView(LoginRequiredMixin, UpdateView):
    model = Bitacoras
    form_class = BitacoraForm
    template_name = 'bitacora/crearbitacora.html'
    success_url = reverse_lazy('app:ListarBitacora')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # Data es la variable que vamos a llevar al front para
        # interactuar con Ajax
        data = {}
        # Try ejecuta la accion y genera excepciones ante los errores
        try:
            # La variable Action evalua si lleva un atributo action el template
            # entonces se ejecuta la siguiente accion
            action = request.POST['action']
            # Si en el templeate action tiene el valor de Search_persons entonces guarda la data en un listado
            if action == 'search_persons':
                data = []
                # Se crea la variable clientes para enviar la tabla personas al templeate filtrado por nombre
                personas = Personas.objects.filter(nombre__icontains=request.POST['busqueda'])[0:5]
                # se recorre la variable para obtener todos los datos
                for i in personas:
                    # se envian los datos sen formato Json
                    item = i.cambioJson()
                    item['value'] = i.nombre
                    data.append(item)
            elif action == 'edit':
                with transaction.atomic():
                    vents = json.loads(request.POST['vents'])
                    bitacora = self.get_object()
                    bitacora.usuario_id = vents['usuario']
                    bitacora.reunion = vents['reunion']
                    bitacora.numero_personas = vents['numero_personas']
                    bitacora.fecha = vents['fecha']
                    bitacora.lugar_encuentro_id = vents['lugar_encuentro']
                    bitacora.observaciones = vents['observaciones']
                    bitacora.save()
                    bitacora.detallebitacora_set.all().delete()
                for i in vents['personas']:
                    detalle = DetalleBitacora()
                    detalle.bitacora_id = bitacora.id
                    detalle.persona_id = i['id']
                    detalle.tapabocas = bool(i['tapabocas'])
                    detalle.distancia = bool(i['distancia'])
                    detalle.observaciones = i['observacionesper']
                    detalle.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_detalle_bitacora(self):
        data = []
        try:
            for i in DetalleBitacora.objects.filter(bitacora_id=self.get_object().id):
                item = i.persona.cambioJson()
                item['tapabocas'] = i.tapabocas
                item['distancia'] = i.distancia
                item['observaciones'] = i.observaciones
                data.append(item)
        except:
            pass
        return data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Edicion Registro'
        context['home'] = reverse_lazy('app:Dashboard')
        context['status_url'] = reverse_lazy('')
        context['status'] = 'Crear registro'
        context['bienvenido'] = 'Crear Registro'
        context['tablapersonas'] = Personas.objects.all()
        context['action'] = 'edit'
        context['detalle'] = json.dumps(self.get_detalle_bitacora())
        return context


class BitacoraDeleteView(LoginRequiredMixin, DeleteView):
    model = Bitacoras
    template_name = 'bitacora/EliminarBitacora.html'
    success_url = reverse_lazy('app:ListarBitacora')
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Eliminacion Bitacora'
        context['home'] = reverse_lazy('app:ListarBitacora')
        context['status_url'] = reverse_lazy('app:EliminarBitacora')
        context['status'] = 'Eliminar registro'
        context['bienvenido'] = 'Eliminar registro'
        context['hora'] = datetime.now().ctime()
        return context

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)


class BitacoraDetallePdfView(View):

    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
        try:
            template = get_template('bitacora/FacturaPdf.html')
            context = {
                'bitacora': Bitacoras.objects.get(pk=self.kwargs['pk']),
                'detalle': DetalleBitacora.objects.filter(bitacora_id=Bitacoras.objects.get(pk=self.kwargs['pk'])),
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/logo.png')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            # si desea descargar el archivo de una vez dejar esta opcion
            # response['Content-Disposition'] = 'attachment; filename="Detalle Bitacora.pdf"'

            # create a pdf
            pisa_status = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response

        except:
            pass

        return HttpResponse(reverse_lazy('app:ListarBitacora'))
