from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from datetime import *
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt, csrf_protect

from core.bitacora.forms import NuevaPersona, EditPersona
from core.bitacora.models import *
from django.views.generic import ListView, CreateView, DeleteView, UpdateView, FormView


# Las vistas genericas dan facilidad para interactuar con los datos de la Basde de Datos, y los maneja
# como objetos.


# Listview para las vistas

class personasListView(LoginRequiredMixin, ListView):
    # La variable model se llama para usar el modelo
    model = Personas
    # template_name donde esta ubicado el archivo base.
    template_name = 'personas/lista.html'

    @method_decorator(csrf_exempt)
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # Sobre escribir metodo post
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'personas':
                data = []
                for i in Personas.objects.all():
                    data.append(i.cambioJson())
            # se envian los datos en Json pero primero se pasan a diccionario en el modelo
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    # la funcion     def get_context_data(self, **kwargs):
    # se usa para pasar mas parametros a la plantilla
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'listado de empleados'
        context['home'] = reverse_lazy('app:lista_personas')
        context['create_url'] = reverse_lazy('app:Nueva_Persona')
        context['status_url'] = reverse_lazy('app:lista_personas')
        context['status'] = 'lista de personas'
        context['bienvenido'] = 'Bienvenido'
        context['hora'] = datetime.now().ctime()

        return context


# CreateView se usa para la insercion de un nuevo registro
class PersonaCreateView(LoginRequiredMixin, CreateView):
    # Se llama al modelo que se usara
    model = Personas
    # el formulario que se usa
    form_class = NuevaPersona
    # el archivo base html
    template_name = 'personas/crear.html'
    # se ejecura la accion que conlleva a la la pagina deseada
    success_url = reverse_lazy('app:lista_personas')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    # se usa para pasar mas parametros a la plantilla
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Nuevo usuario'
        context['home'] = reverse_lazy('app:lista_personas')
        context['status_url'] = reverse_lazy('app:Nueva_Persona')
        context['status'] = 'Crear registro'
        context['bienvenido'] = 'Crear Registro'
        context['hora'] = datetime.now().ctime()
        context['action'] = 'add'
        return context


# actualizar el registrio
class PersonaUpdate(LoginRequiredMixin, UpdateView):
    model = Personas
    form_class = EditPersona
    template_name = 'personas/edit.html'
    success_url = reverse_lazy('app:lista_personas')

    # se usa la funcion dispatch para la validacion unicamente del registrio

    # se usa para pasar mas parametros a la plantilla
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Editar persona'
        context['home'] = reverse_lazy('app:lista_personas')
        context['status_url'] = reverse_lazy('app:Editar_Persona')
        context['status'] = 'Editar Registro'
        context['bienvenido'] = 'Edicion de registro'
        context['hora'] = datetime.now().ctime()

        return context

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)


# se usa para eliminar registros
class PersonaDeleteView(LoginRequiredMixin, DeleteView):
    model = Personas
    template_name = 'personas/delete.html'
    success_url = reverse_lazy('app:lista_personas')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Eliminacion usuario'
        context['home'] = reverse_lazy('app:lista_personas')
        context['status_url'] = reverse_lazy('app:Eliminar_Persona')
        context['status'] = 'Eliminar registro'
        context['bienvenido'] = 'Eliminar registro'
        context['hora'] = datetime.now().ctime()
        return context

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)

        return JsonResponse(data)
