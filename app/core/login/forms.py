from django.contrib.auth.forms import UserCreationForm
from django.forms import *
from django.forms import EmailField

from core.bitacora.models import *


class RegistroUsuario(UserCreationForm):
    class Meta:
        model = user
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'avatar',
            'numero_documento',

        ]
        labels = {
            'username': "nombre de usuario",
            'first_name': 'ingrese  nombre',
            'last_name': 'Ingrese Apellido',
            'email': 'Ingrese email',
            'avatar': ' imagen',
            'numero_documento': 'Ingrese numero de documento'

        }

