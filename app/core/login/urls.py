from django.urls import path, include
from core.login.views import LoginFormView, LogoutFormView, RegistroCreateFormView

app_name = 'login'

urlpatterns = [
    path('',LoginFormView.as_view(), name="login"),
    path('logout/',LogoutFormView.as_view(), name="logout"),
    path('registro/',RegistroCreateFormView.as_view(), name="Registro")
]
