from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from django.http import Http404

from core.bitacora.models import user
from core.login.forms import RegistroUsuario


class LoginFormView(LoginView):
    template_name = 'login.html'

    def dispatch(self, request, *args, **kwargs):
        # si se quiere ir a el inicio de sesion cuando una misma ya este iniciada
        # se realiza una validacion si la sesion esta iniciada entonces redireccionar a listar personas
        if request.user.is_authenticated:
            return redirect("app:Dashboard")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'SRF CONSULTORES'
        context ['url'] = 'Incio de session'
        return context

class LogoutFormView(LogoutView):
    next_page = '/login/'


class RegistroCreateFormView(CreateView):
    model = user
    template_name = "registro.html"
    form_class = RegistroUsuario
    success_url = reverse_lazy('inicio')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # def post(self, request, *args, **kwargs):
    #     data= {}
    #     try:
    #         print(request.POST)
    #         print(request.FILES)
    #
    #     except:
    #         pass


