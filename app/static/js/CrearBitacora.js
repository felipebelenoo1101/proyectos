///////////////////////Inicio parte de registro de persona al detalle//////////////////////////////
// creamos una variable que va a guardar los datos

///////////////////////Inicio parte de registro de persona al detalle//////////////////////////////
// creamos una variable que va a guardar los datos

var tblPersonas;
var vents = {

    pers: {
        usuario: '',
        reunion: '',
        numero_personas: '',
        fecha: '',
        lugar_encuentro: '',
        observaciones: '',
        personas: []
    },
    add: function (item) {
        this.pers.personas.push(item);
        this.list();
    },

    list: function () {
        tblPersonas = $('#tblPersonas').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            data: this.pers.personas,
            columns: [
                {"data": "id"},
                {"data": "nombre"},
                {"data": "apellido"},
                {"data": "tapabocas"},
                {"data": "distancia"},
                {"data": "observacionesper"},

            ],
            columnDefs: [
                {
                    targets: [0],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<a rel="remove" class="btn btn-danger btn-sm" style="color: white"><i class="fas fa-trash"></i></a>';
                    }
                },
                {
                    targets: [-3],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<select name="tapabocas" class="form-control"><option value="0">------------</option>' +
                            ' <option value="1">Con tapabocas</option> ' +
                            '<option value="0">Sin tapabocas</option> </select>';
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<select name="distancia" class="form-control">' +
                            '<option value="0">------------</option> ' +
                            '<option value="1">Conserva distancia</option> ' +
                            '<option value="0"> No conserva distancia </option> </select>';
                    }
                },
                {
                    targets: [-1],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<textarea type="text" name="observacionesper" class="form-control" ></textarea>';
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });
    },
};


$(function () {
    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es'
    });

    // busqueda de personas

    $('input[name="search"]').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_persons',
                    'busqueda': request.term
                },
                dataType: 'json',
            }).done(function (data) {
                response(data);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + ': ' + errorThrown);
            }).always(function (data) {

            });
        },
        delay: 500,
        minLength: 1,
        select: function (event, ui) {
            event.preventDefault();
            console.clear();
            ui.item.tapabocas = '';
            ui.item.distancia = '';
            ui.item.observacionesper = '';
            vents.add(ui.item);
            console.log(vents.pers);

            $(this).val('');
        }
    });

    // Eliminar registros de la tabla
    $('#tblPersonas tbody')
        .on('click', 'a[rel="remove"]', function () {
            var tr = tblPersonas.cell($(this).closest('td,li')).index();
            vents.pers.personas.splice(tr.row, 1);
            vents.list();
        });

    $('#tbldelate').on('click', function () {
        if (vents.pers.personas.length === 0) return false;
        submit_eliminar_datos_tb('Eliminacion de pre-registros', '¿Desea Eliminar todos los pre-registros ?', function () {
            vents.pers.personas = [];
            vents.list();


        })

    });
/// Funciones para agregar valores ///
    $('#tblPersonas tbody').on('change', 'select[name="tapabocas"]', function () {
        var tapabocas = parseInt($(this).val());
        var tr = tblPersonas.cell($(this).closest('td', 'li')).index();
        console.log(tr)
        vents.pers.personas[tr.row].tapabocas = tapabocas;

    });
    $('#tblPersonas tbody').on('change', 'select[name="distancia"]', function () {
        var distancia = parseInt($(this).val());
        var tr = tblPersonas.cell($(this).closest('td', 'li')).index();
        console.log(tr)
        vents.pers.personas[tr.row].distancia = distancia;

    });
    $('#tblPersonas tbody').on('change', 'textarea[name="observacionesper"]', function () {
        var observacionesper = $(this).val();
        var tr = tblPersonas.cell($(this).closest('td', 'li')).index();
        console.log(tr)
        vents.pers.personas[tr.row].observacionesper = observacionesper;

    });

    // enviar datos a los modelos o DB
    $('form').on('submit', function (e) {
        e.preventDefault()

        vents.pers.usuario = $('select[name="usuario"]').val();
        vents.pers.reunion = $('input[name="reunion"]').val();
        vents.pers.numero_personas = $('input[name="numero_personas"]').val();
        vents.pers.fecha = $('input[name="fecha"]').val();
        vents.pers.lugar_encuentro = $('select[name="lugar_encuentro"]').val();
        vents.pers.observaciones = $('textarea[name="observaciones"]').val();


        //se crea una variable para ingresar los datos en un arreglo
        //this hace referencia al fomulario y sis datos
        //serializeArray guarda los datos en un arreglo
        var parametros = new FormData();
        parametros.append('action', $('input[name="action"]').val());
        parametros.append('vents', JSON.stringify(vents.pers));

        submit_ajax(window.location.pathname, parametros, 'Notificacion', '¿Desea enviar datos del formulario?', function () {
            location.href = '/app/bitacoralista';
        });


    });
    vents.list();

});