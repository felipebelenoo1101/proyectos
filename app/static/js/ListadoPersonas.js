var tblpersonas;
$(function () {
    tblpersonas = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'personas'
            },
            dataSrc: ""
        },
        columns: [

            {"data": "id"},
            {"data": "nombre"},
            {"data": "apellido"},
            {"data": "tipo_documento.tipo_documento"},
            {"data": "cedula"},
            {"data": "correo"},
            {"data": "telefono"},
            {"data": "Opciones"},

        ],
        columnDefs: [
            {
                targets: ['_all'],
                className: 'mdc-data-table__cell'
            },
            {
                targets: [-1],
                class: "text-center",
                orderable: false,
                render: function (data, type, row) {
                    var botones = ' <a type="button" href="/app/deletePersona/' + row.id + '/" class="btn btn-danger btn-xs"><i\n' +
                        '                        class="fa fa-trash-alt"></i></a>\n'
                    botones += '<button type="button" class="btn btn-dark btn-xs"><i class="fa  fa-file-pdf"></i></button>\n'
                    botones += '<a href="/app/editPersona/' + row.id + '/" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'

                    return botones;
                }
            },
            {
                targets: [-7],
                class: "text-center",
                orderable: false,
                render: function (data, type, row) {

                    return data;
                }
            },
        ],

        initComplete: function (settings, json) {

        }
    });


});