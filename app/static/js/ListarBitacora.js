var tblbitacora;
$(function () {
    tblbitacora = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'buscarDatos'
            },
            dataSrc: ""
        },
        columns: [

            {"data": "id"},
            {"data": "reunion"},
            {"data": "numero_personas"},
            {"data": "fecha"},
            {"data": "lugar_encuentro.lugar"},
            {"data": "observaciones"},
            {"data": "usuario.first_name"},
            {"data": "usuario.last_name"},
            {"data": "Opciones"},

        ],
        columnDefs: [

            {
                targets: [-1],
                class: "text-center",
                orderable: false,
                render: function (data, type, row) {
                    var botones = '<button rel="detalle" type="button" class="btn btn-light btn-xs"><i class="fa fa-search"></i></button>'
                    botones += '<a href="/app/bitacoradelete/'+row.id+'/" class="btn btn-danger btn-xs"><i class="fa fa-trash-alt"></i></a>'
                    botones += '<a href="/app/bitacorapdf/'+row.id+'/" class="btn btn-dark btn-xs"><i class="fa fa-file-pdf"></i></a>'
                    botones += '<a href="/app/bitacoraedit/'+row.id+'/" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'
                    return botones;
                }
            },
            {
                targets: [-7],
                class: "text-center",
                orderable: false,
                render: function (data, type, row) {

                    return data;
                }
            },
        ],

        initComplete: function (settings, json) {

        }
    });

    $('#data tbody')
        .on('click', 'button[rel="detalle"]', function () {
            var tr = tblbitacora.cell($(this).closest('td,li')).index();
            var data = tblbitacora.row(tr.row).data();
            console.log(data)
            $('#tblPersonasDetalle').DataTable({
                responsive: true,
                autoWidth: false,
                destroy: true,
                deferRender: true,
                ajax: {
                    url: window.location.pathname,
                    type: 'POST',
                    data: {
                        'action': 'buscarDetalle',
                        'id': data.id,
                    },
                    dataSrc: ""
                },
                columns: [

                    {"data": "bitacora.reunion"},
                    {"data": "persona.nombre"},
                    {"data": "persona.apellido"},
                    {"data": "tapabocas"},
                    {"data": "distancia"},
                    {"data": "observaciones"},
                ],
                columnDefs: [
                    {
                        targets: [-3, -2],
                        class: 'text-center',
                        orderable: false,
                        render: function (data, type, row) {
                            // es Facil rimero estas en la columna  ambas le poes que legara el vlaorbooleano
                            // entonces data tendtra true o false
                            // basta poner asi data que si es true pasa sino sera falso
                            // para que tengas una mejor idea has un console asi
                            // cmo ambas son boleanos debe ser el valor que caiga / asi funciona para las dos si quieres las separas

                            console.log(data);
                            if (data) {
                                return '<span class="badge badge-success">Si</span>';
                            }
                            return '<span class="badge badge-danger">No</span>';
                        }
                    },
                ],

                initComplete: function (settings, json) {
                }
            });


            $('#modalDetalle').modal('show');
        });


});