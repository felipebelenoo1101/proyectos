function alerta_error(obj) {
    $.each(obj, function (key, value) {
        console.log(key);
        console.log(value);
    });
}


function submit_ajax(url, parametros, title, content, callback) {
    $.confirm({
            theme: 'Modern',
            title: title,
            icon: 'fa fa-info',
            content: content,
            columnClass: 'medium',
            buttons: {
                info: {
                    text: "SI",
                    btnClass: 'btn-primary',
                    action: function () {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: parametros,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                        }).done(function (data) {
                            if (!data.hasOwnProperty('error')) {
                                callback();
                                return false;
                            }
                            alerta_error(data.error)
                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            alert(textStatus + ': ' + errorThrown);
                        }).always(function (data) {

                        });
                    }
                },
                danger: {
                    text: 'NO',
                    btnClass: 'btn-red',
                    action: function () {

                    }
                },
            }
        }
    )
    ;

}

function submit_eliminar_datos_tb(title, content, callback) {
    $.confirm({
            theme: 'Modern',
            title: title,
            icon: 'fa fa-info',
            content: content,
            columnClass: 'medium',
            buttons: {
                info: {
                    text: "SI",
                    btnClass: 'btn-primary',
                    action: function () {
                        callback();
                    }
                },
                danger: {
                    text: 'NO',
                    btnClass: 'btn-red',
                    action: function () {

                    }
                },
            }
        }
    )
    ;

}
